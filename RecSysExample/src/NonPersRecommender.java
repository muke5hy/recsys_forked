import java.io.File;
import it.unimi.dsi.fastutil.longs.LongArraySet;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.longs.LongSortedSet;
import org.grouplens.lenskit.collections.LongUtils;
import org.grouplens.lenskit.cursors.Cursor;
import org.grouplens.lenskit.data.dao.EventDAO;
import org.grouplens.lenskit.data.dao.ItemDAO;
import org.grouplens.lenskit.data.dao.ItemEventDAO;
import org.grouplens.lenskit.data.dao.UserDAO;
import org.grouplens.lenskit.data.dao.UserEventDAO;
import org.grouplens.lenskit.data.event.Event;
import org.grouplens.lenskit.data.history.UserHistory;
import org.grouplens.lenskit.data.pref.PreferenceDomain;
import org.grouplens.lenskit.data.pref.PreferenceDomainBuilder;
import org.grouplens.lenskit.eval.data.CSVDataSource;
import org.grouplens.lenskit.eval.data.CSVDataSourceBuilder;

public class NonPersRecommender {
    
    int movies[];
    File file;
    CSVDataSource datasource;
    
    public NonPersRecommender(File file) {
        movies = new int[5];
        this.file = file;
    }
    
    public void getData() {
        
        //Build preference domain
        PreferenceDomain prefdom = new PreferenceDomainBuilder(1, 5).
                setPrecision(1).
                build();
        //Get the into the data source
        datasource = new CSVDataSourceBuilder(file).
                setName("nonpers").
                setDomain(prefdom).
                build();
        
    }
    
    public void eventExample() {
        
        EventDAO eventdao = datasource.getEventDAO();
        Cursor<Event> mycursor = eventdao.streamEvents();
        
        System.out.println("Events:");
        for(Event oneevent : mycursor) {
            System.out.println(oneevent.getItemId() +  ", " + oneevent.getUserId());
        }
        
    }
    
    public void itemExample() {
        
        ItemDAO itemdao = datasource.getItemDAO();
        LongSet itemset = itemdao.getItemIds();
        
        System.out.println("Item Ids:");
        for(Long item : itemset) {
            System.out.println(item);
        }
    }
    
    public void userExample() {
        UserDAO userdao = datasource.getUserDAO();
        LongSet userset = userdao.getUserIds();
        
        System.out.println("User Ids:");
        for(Long user : userset) {
            System.out.println(user);
        }
    }
    
    public void userEventExample() {
        UserDAO userdao = datasource.getUserDAO();
        LongSet userset = userdao.getUserIds();
        UserEventDAO usereventdao = datasource.getUserEventDAO();
        
        System.out.println("User Event Example: ");
        for(Long user: userset) {
            System.out.print("User: " + user + " ");
            UserHistory<Event> userdata = usereventdao.getEventsForUser(user);
            LongSet itemset = userdata.itemSet();
            System.out.println(itemset.toString());
        }
        
    }
    
    public void itemEventExample() {
        ItemDAO itemdao = datasource.getItemDAO();
        LongSet itemset = itemdao.getItemIds();
        ItemEventDAO itemeventdao = datasource.getItemEventDAO();
        
        System.out.println("Item Event Example: ");
        for(Long item : itemset) {
            System.out.print("Item: " + item + " ");
            LongSet userset = itemeventdao.getUsersForItem(item);
            System.out.println(userset.toString());
        }
    }
    
    public void longSetIntersectionExample() {
        long array1[] = {1,2,3,4,5};
        long array2[] = {3,4,5,6,7};
        LongSet set1 = new LongArraySet(array1);
        LongSet set2 = new LongArraySet(array2);
        LongSortedSet set3, set4;
        
        System.out.println("Set 1: " + set1.toString());
        System.out.println("Set 2: " + set2.toString());
        
        set3 = LongUtils.setDifference(set1, set2);
        set4 = LongUtils.setDifference(set2, set1);
        
        for(Long item : set3) {
            if(set1.contains(item))
                set1.remove(item);
        }
        
        for(Long item : set4) {
            if(set1.contains(item)) {
                set1.remove(item);
            }
        }
        
        System.out.println("Set 1 Now contains intersection: " + set1.toString());
 
    }

}

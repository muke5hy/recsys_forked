import java.io.File;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {   
        //Your toy data set
        File myfile = new File("toysample.csv");
        
        //Recommender class, gets data and from CSV file and
        //manipulates data
        NonPersRecommender rec = new NonPersRecommender(myfile);
        
        //Invoke the method to get data from CSV file
        rec.getData();
        
        //Basic event example
        //In simple words: Goes through all lines of CSV once
        rec.eventExample();
        
        //Basic Item example
        //In simple words: Shows all the items in the CSV file
        rec.itemExample();
        
        //Basic User example
        //In simple words: Shows all the users in the CSV file
        rec.userExample();
        
        //Basic User Event example
        //In simple words: Shows movies rated by a user for every user
        rec.userEventExample();
        
        //Basic Item Event example
        //In simple words: Shows for every movie, who all has rated it
        rec.itemEventExample();
        
        //Basic LongSet intersection example
        //In simple words: Shows you the intersection between two LongSets
        //NOTE: Not related to recommender class, but I'm lazy to create
        // a new class to show you this example
        rec.longSetIntersectionExample();
        
    }
}
